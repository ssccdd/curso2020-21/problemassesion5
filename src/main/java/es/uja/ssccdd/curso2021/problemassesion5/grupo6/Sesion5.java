/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo6;

import es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.FabricanteVacuna;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.PACIENTES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.ENFERMEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.ALMACENES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.TOTAL_TIPOS_FABRICANTES;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion5 {

    public static void main(String[] args) {

        // Variables aplicación
        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();
        CompletionService<List<DosisVacuna>> service = new ExecutorCompletionService<>(executor);

        ArrayList<Paciente> pacientes = new ArrayList<>();
        ArrayList<Paciente> pacientesInmunizados = new ArrayList<>();
        ReentrantLock cerrojoPacientes = new ReentrantLock();
        ReentrantLock cerrojoPacientesInmunizados = new ReentrantLock();
        ReentrantLock cerrojoIDDosis = new ReentrantLock();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");
        System.out.println("HILO-Principal Generando almacenes");

        for (int i = 0; i < ALMACENES_A_GENERAR; i++) {
            FabricanteVacuna fabricante = FabricanteVacuna.getFabricanteOrdinal(i % TOTAL_TIPOS_FABRICANTES);
            AlmacenMedico alm = new AlmacenMedico(i, fabricante, cerrojoIDDosis);
            service.submit(alm);
        }

        System.out.println("HILO-Principal Generando pacientes");

        for (int i = 0; i < PACIENTES_A_GENERAR; i++) {
            pacientes.add(new Paciente(i));
        }

        System.out.println("HILO-Principal Generando enfermeros");

        for (int i = 0; i < ENFERMEROS_A_GENERAR; i++) {
            Enfermero enfermero = new Enfermero(i, pacientes, pacientesInmunizados, cerrojoPacientes, cerrojoPacientesInmunizados, service);
            executor.execute(enfermero);
        }

        System.out.println("HILO-Principal Espera para parar a los enfermeros");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion5.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion5.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("HILO-Principal Inmunizados " + pacientesInmunizados.size() + " pacientes.");
    }

}
