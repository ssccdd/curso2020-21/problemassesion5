/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo4;

import es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.TipoPlato;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.TOTAL_TIPOS_PLATOS;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.TIEMPO_ESPERA_RESTAURANTE;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Restaurante implements Callable<List<Plato>> {

    private final int iD;
    private final ReentrantLock cerrojo;
    private static int siguienteID = 0;

    public Restaurante(int iD, ReentrantLock cerrojo) {
        this.iD = iD;
        this.cerrojo = cerrojo;
    }

    private static int getSiguienteID() {
        return siguienteID++;
    }

    @Override
    public List<Plato> call(){

        System.out.println("Restaurante " + iD + " ha empezado.");
        
        ArrayList<Plato> platos = new ArrayList<>();
        
        for (int i = 0; i < TOTAL_TIPOS_PLATOS; i++) {
            
            cerrojo.lock();
            int iD = getSiguienteID();
            cerrojo.unlock();
            
            platos.add(new Plato(iD, TipoPlato.getPlatoOrdinal(i)));
            
            try {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_RESTAURANTE);
            } catch (InterruptedException ex) {
                //No hacemos nada porque el hilo no se interrumpirá.
                Logger.getLogger(Restaurante.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
        
        System.out.println("Restaurante " + iD + " ha terminado.");

        return platos;
        
    }

}
