/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo1;

import es.uja.ssccdd.curso2021.problemassesion5.grupo1.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo1.Constantes.VALOR_CONSTRUCCION;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo1.Sesion5.generaID_SISTEMA;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class NuevoProceso implements Runnable {
    private final String iD;
    private final CompletionService<Proceso> nuevoProceso;
    private final List<Future<?>> listaTareas;

    public NuevoProceso(String iD, CompletionService<Proceso> nuevoProceso, List<Future<?>> listaTareas) {
        this.iD = iD;
        this.nuevoProceso = nuevoProceso;
        this.listaTareas = listaTareas;
    }

    @Override
    public void run() {
        TipoProceso tipoProceso = TipoProceso.getTipoProceso(aleatorio.nextInt(VALOR_CONSTRUCCION));
        System.out.println("TAREA-" + iD + " crea un proceso " + tipoProceso);

        try {

            crearProceso(tipoProceso);

        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " se ha CANCELADO");
        }
    }

    public String getiD() {
        return iD;
    }

    private void crearProceso(TipoProceso tipoProceso) throws InterruptedException {
        Future<Proceso> tarea;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        CrearProceso crearProceso = new CrearProceso("CrearProceso("+generaID_SISTEMA()+")", tipoProceso);
        
        tarea = nuevoProceso.submit(crearProceso);
        listaTareas.add(tarea);
    }
}
