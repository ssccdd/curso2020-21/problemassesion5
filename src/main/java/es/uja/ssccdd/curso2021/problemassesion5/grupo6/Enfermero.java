/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo6;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.TIEMPO_ESPERA_ENFERMERO_MAX;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.TIEMPO_ESPERA_ENFERMERO_MIN;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.DOSIS_POR_PACIENTE;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.random;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Enfermero implements Runnable {

    private final int iD;
    private final ArrayList<Paciente> pacientes;
    private final ArrayList<Paciente> pacientesInmunizados;
    private final ReentrantLock cerrojoPacientes;
    private final ReentrantLock cerrojoPacientesInmunizados;
    private final CompletionService<List<DosisVacuna>> comService;

    public Enfermero(int iD, ArrayList<Paciente> pacientes, ArrayList<Paciente> pacientesInmunizados, ReentrantLock cerrojoPacientes, ReentrantLock cerrojoPacientesInmunizados, CompletionService<List<DosisVacuna>> comService) {
        this.iD = iD;
        this.pacientes = pacientes;
        this.pacientesInmunizados = pacientesInmunizados;
        this.cerrojoPacientes = cerrojoPacientes;
        this.cerrojoPacientesInmunizados = cerrojoPacientesInmunizados;
        this.comService = comService;
    }

    @Override
    public void run() {

        boolean interrumpido = false;
        ArrayList<Paciente> copiaLocalPacientes = new ArrayList<>();

        while (!interrumpido) {

            try {
                Future<List<DosisVacuna>> futuroDosis = comService.take();
                List<DosisVacuna> dosis = futuroDosis.get();
                int indiceDosis = 0;

                while (indiceDosis < dosis.size() && !interrumpido) {
                    //sabemos que las dosis viene en número par
                    //podemos saltarnos esa comprobación

                    Paciente paciente = null;
                    cerrojoPacientes.lock();
                    {
                        if (pacientes.size() > 0) {
                            paciente = pacientes.remove(0);
                        } else { //Si no hay pacientes salgo como si hubiera sido interrumpido
                            interrumpido = true;
                        }
                    }
                    cerrojoPacientes.unlock();

                    if (paciente != null) {
                        for (int i = 0; i < DOSIS_POR_PACIENTE; i++) {
                            paciente.addDosisVacuna(dosis.get(indiceDosis++));

                            try {
                                int tiempoEspera = random.nextInt(TIEMPO_ESPERA_ENFERMERO_MAX - TIEMPO_ESPERA_ENFERMERO_MIN) + TIEMPO_ESPERA_ENFERMERO_MIN;
                                TimeUnit.MILLISECONDS.sleep(tiempoEspera);
                            } catch (InterruptedException ex) {
                                //Este catch permite que se cambie la variable pero que siga procesando el paciente actual.
                                interrumpido = true;
                            }

                        }

                        cerrojoPacientesInmunizados.lock();
                        pacientesInmunizados.add(paciente);
                        cerrojoPacientesInmunizados.unlock();

                        copiaLocalPacientes.add(paciente);

                    }
                }

                //El catch esta aquí abajo para saltar el bucle si se nos interrumpe durante el take()                
            } catch (InterruptedException ex) {
                interrumpido = true;
            } catch (ExecutionException ex) {//Si salta una excepción en la ejecución lo muestro por consola
                Logger.getLogger(Enfermero.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        imprimirInfo(copiaLocalPacientes);

    }

    /**
     * Imprime una cadena con la información de la clase
     * @param trabajo pacientes inmunizados
     */
    public void imprimirInfo(ArrayList<Paciente> trabajo) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\n\nEnfermero ").append(iD).append(", inmunizados ").append(trabajo.size()).append(" pacientes");

        for (Paciente pac : trabajo) {
            mensaje.append("\n\t").append(pac.toString());
        }

        System.out.println(mensaje.toString());
    }

}
