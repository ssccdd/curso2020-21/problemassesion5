/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.ARCHIVOS;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.CREAR_ARCHIVO;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.ESPERA_FINALIZACION;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.INICIO;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion5 {
    static int ID_SISTEMA = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ScheduledExecutorService ejecucionSistema;
        CompletionService<Archivo> nuevoArchivo;
        ExecutorService ejecucion;
        ArrayList<Future<Informe>> listaTareasGestores;
        CountDownLatch esperaFinalizacion;
        List<Future<?>> listaTareas;
        Future<?> tarea;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        int numNuevoProceso = aleatorio.nextInt(NUM_GESTORES) + 1;
        ejecucionSistema = Executors.newScheduledThreadPool(NUM_GESTORES + numNuevoProceso + 1);
        ejecucion = Executors.newFixedThreadPool(ARCHIVOS);
        nuevoArchivo = new ExecutorCompletionService(ejecucion);
        esperaFinalizacion = new CountDownLatch(ESPERA_FINALIZACION);
        listaTareas = new ArrayList();
        listaTareasGestores = new ArrayList();

        // Se añade las tareas para crear procesos que se repetirá cíclicamente
        System.out.println("HILO(Principal) Ejecuta " + numNuevoProceso + " tareas para crear procesos");
        for(int i = 0; i < numNuevoProceso; i++) {
            NuevoArchivo crearArchivo = new NuevoArchivo("NUEVO_PROCESO("+i+")", nuevoArchivo,listaTareas);
            tarea = ejecucionSistema.scheduleAtFixedRate(crearArchivo, INICIO, CREAR_ARCHIVO, TimeUnit.SECONDS);
            listaTareas.add(tarea);
        }
        
        // Creamos los gestores y los ejecutamos
        for(int i = 0; i < NUM_GESTORES; i++) {
            Future<Informe> tareaGestor;
            GestorAlmacenamiento gestor = new GestorAlmacenamiento("GESTOR(" + i + ")", nuevoArchivo);
            tareaGestor = ejecucionSistema.submit(gestor);
            listaTareas.add(tareaGestor); // Se añade para la cancelación
            listaTareasGestores.add(tareaGestor);
        }
        
        // Se crea la tarea de cancelación que se ejecutará pasado el TIEMPO_ESPERA
        // y finalizará las tareas que estén activas
        TareaFinalizacion fin = new TareaFinalizacion(listaTareas,esperaFinalizacion);
        ejecucionSistema.schedule(fin, TIEMPO_ESPERA, TimeUnit.MINUTES);
        System.out.println("HILO(Principal) Espera el tiempo establecido");
        esperaFinalizacion.await();
        
        // Cerramos los marcos de ejecucion
        ejecucionSistema.shutdownNow();
        ejecucion.shutdownNow();

        ejecucionSistema.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Mostrar el estado de sistema distribuido
        System.out.println("(HILO_PRINCIPAL) Estado del sistema");
        for(int i = 0; i < NUM_GESTORES; i++) {
            Future<Informe> tareaGestor = listaTareasGestores.get(i);
            if( !tareaGestor.isCancelled() ) {
                Informe informe = tareaGestor.get();
                System.out.println(informe);
            } else 
                System.out.println("El Gestor " + i + " fue CANCELADO y no se generó el informe");
        }

        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
    public static int generaID_SISTEMA() {
        return ID_SISTEMA++;
    }
}
