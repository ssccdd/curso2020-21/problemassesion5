/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.random;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.TIEMPO_ESPERA_REPARTIDOR_MIN;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.TIEMPO_ESPERA_REPARTIDOR_MAX;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Repartidor implements Runnable {

    private final int iD;
    private final ArrayList<MenuReparto> pedidos;
    private final ReentrantLock cerrojo;
    private final CompletionService<List<Plato>> comService;
    private static int siguienteID = 0;

    public Repartidor(int iD, ArrayList<MenuReparto> pedidos, ReentrantLock cerrojo, CompletionService<List<Plato>> comService) {
        this.iD = iD;
        this.pedidos = pedidos;
        this.cerrojo = cerrojo;
        this.comService = comService;
    }
    
    private static int getSiguienteID() {
        return siguienteID++;
    }

    @Override
    public void run() {

        ArrayList<MenuReparto> listaLocalMenús = new ArrayList<>();
        boolean interrumpido = false;
        System.out.println("Repartidor " + iD + " ha empezado.");

        while (!interrumpido) {

            try {
                //No compruebo si es nulo porque take me asegura que el futuro no será nulo.
                Future<List<Plato>> futPlatos = comService.take();
                List<Plato> platos = futPlatos.get();
                
                cerrojo.lock();
                int idMenú = getSiguienteID();
                cerrojo.unlock();
                
                MenuReparto menuNuevo = new MenuReparto(idMenú);
                
                for (Plato plato : platos) {
                    menuNuevo.addPlato(plato);
                }
                
                listaLocalMenús.add(menuNuevo);
                
                cerrojo.lock();
                pedidos.add(menuNuevo);
                cerrojo.unlock();
                
                int espera = random.nextInt(TIEMPO_ESPERA_REPARTIDOR_MAX - TIEMPO_ESPERA_REPARTIDOR_MIN) + TIEMPO_ESPERA_REPARTIDOR_MIN;
                TimeUnit.MILLISECONDS.sleep(espera);
            } catch (InterruptedException ex) {
                interrumpido = true;
            } catch (ExecutionException ex) {
                //Si salta una excepción en la ejecución lo muestro por consola
                Logger.getLogger(Repartidor.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        imprimirDatos(listaLocalMenús);

    }

    /**
     * Imprimr la información del hilo
     * @param trabajoHecho  menús rellenos
     */
    private void imprimirDatos(ArrayList<MenuReparto> trabajoHecho) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nRepartidor ").append(iD).append(" rellenados ").append(trabajoHecho.size()).append(" menús.");

        for (MenuReparto pedido : trabajoHecho) {
            mensaje.append("\n\t").append(pedido.toString());
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
