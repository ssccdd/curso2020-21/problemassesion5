/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo2;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo2.Sesion5.generaID_SISTEMA;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class NuevoProceso implements Runnable {
    private final String iD;
    private final CompletionService<Proceso> nuevoProceso;
    private final List<Future<?>> listaTareas;

    public NuevoProceso(String iD, CompletionService<Proceso> nuevoProceso, List<Future<?>> listaTareas) {
        this.iD = iD;
        this.nuevoProceso = nuevoProceso;
        this.listaTareas = listaTareas;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " crea un proceso ");

        try {

            crearProceso();

        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " se ha CANCELADA");
        }
    }

    public String getiD() {
        return iD;
    }

    private void crearProceso() throws InterruptedException {
        Future<Proceso> tarea;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        CrearProceso crearProceso = new CrearProceso("CrearProceso("+generaID_SISTEMA()+")");
        
        tarea = nuevoProceso.submit(crearProceso);
        listaTareas.add(tarea);
    }
}
