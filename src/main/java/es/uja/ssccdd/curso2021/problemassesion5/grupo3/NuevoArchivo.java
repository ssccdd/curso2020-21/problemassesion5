/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Sesion5.generaID_SISTEMA;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.Future;

/**
 *
 * @author pedroj
 */
public class NuevoArchivo implements Runnable {
    private final String iD;
    private final CompletionService<Archivo> nuevoArchivo;
    private final List<Future<?>> listaTareas;

    public NuevoArchivo(String iD, CompletionService<Archivo> nuevoArchivo, List<Future<?>> listaTareas) {
        this.iD = iD;
        this.nuevoArchivo = nuevoArchivo;
        this.listaTareas = listaTareas;
    }

    @Override
    public void run() {
        System.out.println("TAREA-" + iD + " crea un archivo ");

        try {

            crearProceso();

        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " se ha CANCELADA");
        }
    }

    public String getiD() {
        return iD;
    }

    private void crearProceso() throws InterruptedException {
        Future<Archivo> tarea;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        CrearArchivo crearArchivo = new CrearArchivo("CrearProceso("+generaID_SISTEMA()+")");
        
        tarea = nuevoArchivo.submit(crearArchivo);
        listaTareas.add(tarea);
    }
}
