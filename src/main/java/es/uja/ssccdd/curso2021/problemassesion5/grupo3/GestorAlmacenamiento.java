/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo3;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.ARCHIVOS;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.ESPACIO_INSUFICIENTE;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.MAXIMO;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.MAXIMO_ALMACENAMIENTO;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.MINIMO;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.MINIMO_ALMACENAMIENTO;
import es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.TipoArchivo;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo3.Sesion5.generaID_SISTEMA;
import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorAlmacenamiento implements Callable<Informe> {
    private final String iD;
    private final ArrayList<UnidadAlmacenamiento> listaDeAlmacenamiento;
    private final ArrayList<Archivo> noAsignados;
    private int numArchivos;
    private Archivo archivo;
    private final CompletionService<Archivo> nuevoArchivo;

    public GestorAlmacenamiento(String iD, CompletionService<Archivo> nuevoArchivo) {
        this.iD = iD;
        this.nuevoArchivo = nuevoArchivo;
        this.listaDeAlmacenamiento = new ArrayList();
        this.noAsignados = new ArrayList();
        this.numArchivos = 0;
        this.archivo = null;
    }

    @Override
    public Informe call() throws Exception {
        Informe informe = new Informe(iD);
        
        System.out.println("TAREA-" + iD + " Empieza su ejecución");
        
        inicializaGestor();
        
        try {
            // Hasta que se alcance el número de archivos o sea interrumpido
            while( numArchivos < ARCHIVOS ) 
                asignarArchivos();
            
            System.out.println("Ha finalizado la ejecución del TAREA-" + iD);
        } catch (InterruptedException | ExecutionException ex) {
            informe.setCancelado(true);
            System.out.println("TAREA-" + iD + " ha sido CANCELADA " + ex);
        } finally {
            // Completa la finbalización del gestor
            finalizacion(informe);
        }
        
        return informe;
    }
    
    private void inicializaGestor() {
        int capacidad;
        
        int totalUnidadesAlmacenamiento = MINIMO + aleatorio.nextInt(MAXIMO - MINIMO + 1);
        for(int j = 0; j < totalUnidadesAlmacenamiento; j++) {
            // Generamos la capacidad de la unidad de almacenamiento
            capacidad = MINIMO_ALMACENAMIENTO + aleatorio.nextInt(MAXIMO_ALMACENAMIENTO - 
                                                                      MINIMO_ALMACENAMIENTO + 1);
            
            listaDeAlmacenamiento.add(new UnidadAlmacenamiento(generaID_SISTEMA(),capacidad));
        }
    }
    
    private void asignarArchivos() throws InterruptedException, ExecutionException {
        Future<Archivo> archivoCreado;
        int tiempo;
        archivo = null;
        
        // Lo primero es comprobar si se ha solicitado la interrupción de la tarea
        if ( Thread.interrupted() )
            throw new InterruptedException();
        
        // Asignamos el archivo a la primera unidad de almacenamiento disponible
        // si es posible
        archivoCreado = nuevoArchivo.take();
        archivo = archivoCreado.get();
        tiempo = tiempoAsignacion(archivo.getTipoArchivo());
        System.out.println("TAREA-" + iD + " Asignando " + archivo + " tiempo necesario " + tiempo + " segundos");
        
        boolean asignado = false;
        int indice = 0;
        while ((indice < listaDeAlmacenamiento.size()) && !asignado) {
            if (listaDeAlmacenamiento.get(indice).addArchivo(archivo) != ESPACIO_INSUFICIENTE) {
                asignado = true;
                archivo = null;
            } else 
                indice++;
        }

        // Si no se ha podido almacenar el archivo se anota
        if(!asignado) {
            noAsignados.add(archivo);
            numArchivos++;
        }

        // Simulamos por último el tiempo de asignación porque si se interrumpe
        // ya hemos completado la operación de asignación
        TimeUnit.SECONDS.sleep(tiempo);
    }
    
    private void finalizacion(Informe informe) {
        // registramos el archivo si no se ha asignado
        if( archivo != null )
            noAsignados.add(archivo);
        
        informe.setListaDeAlmacenamiento(listaDeAlmacenamiento);
        informe.setListaNoAsignados(noAsignados);
    }
    
    public String getiD() {
        return iD;
    }
    
    private int tiempoAsignacion(TipoArchivo tipoArchivo) {
        return MAXIMO + tipoArchivo.ordinal();
    }
}
