/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo5;

import es.uja.ssccdd.curso2021.problemassesion5.grupo4.Repartidor;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.TIEMPO_ESPERA_MAQUINA_MAX;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.TIEMPO_ESPERA_MAQUINA_MIN;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.random;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class MaquinaPostProcesado implements Runnable {

    private final int iD;
    private final ArrayList<Modelo> modelosFinalizados;
    private final ReentrantLock bloqueoModelosFinalizados;
    private final CompletionService<List<Modelo>> comService;

    public MaquinaPostProcesado(int iD, ArrayList<Modelo> modelosFinalizados, ReentrantLock bloqueoModelosFinalizados, CompletionService<List<Modelo>> comService) {
        this.iD = iD;
        this.modelosFinalizados = modelosFinalizados;
        this.bloqueoModelosFinalizados = bloqueoModelosFinalizados;
        this.comService = comService;
    }

    @Override
    public void run() {

        int modelosProcesados = 0;
        ArrayList<Modelo> copiaLocalModelos = new ArrayList<>();
        boolean interrumpido = false;

        System.out.println("Maquina de postprocesado " + iD + " iniciada");

        while (!interrumpido) {

            try {
                Future<List<Modelo>> futModelos = comService.take();

                List<Modelo> modelos = futModelos.get();

                for (Modelo modelo : modelos) {

                    try {
                        int tiempoEspera = random.nextInt(TIEMPO_ESPERA_MAQUINA_MAX - TIEMPO_ESPERA_MAQUINA_MIN) + TIEMPO_ESPERA_MAQUINA_MIN;
                        TimeUnit.MILLISECONDS.sleep(tiempoEspera);
                    } catch (InterruptedException ex) {
                        //Este catch permite que se cambie la variable pero que siga procesando la tanda actual.
                        interrumpido = true;
                    }

                    bloqueoModelosFinalizados.lock();
                    {
                        modelosFinalizados.add(modelo);
                    }
                    bloqueoModelosFinalizados.unlock();

                    copiaLocalModelos.add(modelo);
                    modelosProcesados++;

                }

                //El catch esta aquí abajo para saltar el bucle de modelos si se nos interrumpe durante el take() 
            } catch (InterruptedException ex) {
                interrumpido = true;
            } catch (ExecutionException ex) {//Si salta una excepción en la ejecución lo muestro por consola
                Logger.getLogger(Repartidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        imprimirDatos(copiaLocalModelos);

    }

    /**
     * Imprimir la información del hilo
     * @param trabajoHecho  modelos procesados
     */
    private void imprimirDatos(ArrayList<Modelo> trabajoHecho) {
        StringBuilder mensaje = new StringBuilder();
        mensaje.append("\nMaquina de postprocesado ").append(iD).append(" procesados ").append(trabajoHecho.size()).append(" modelos.");

        int contador = 1;
        mensaje.append("\n\t");

        for (Modelo mod : trabajoHecho) {
            mensaje.append(mod.toString());
            if (contador != 0 && (contador++ % 10) == 0) {
                mensaje.append("\n\t");
            }
        }

        mensaje.append("\n");

        System.out.println(mensaje.toString());
    }

}
