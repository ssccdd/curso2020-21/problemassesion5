/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo5;

import es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.CalidadImpresion;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.INGENIEROS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.VALOR_GENERACION;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.MAQUINAS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion5 {

    public static void main(String[] args) throws InterruptedException {

        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();
        CompletionService<List<Modelo>> service = new ExecutorCompletionService<>(executor);

        ReentrantLock bloqueoIngenieros = new ReentrantLock();
        ReentrantLock bloqueoMaquinas = new ReentrantLock();
        ArrayList<Modelo> modelosFinalizados = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando ingenieros");

        for (int i = 0; i < INGENIEROS_A_GENERAR; i++) {

            int aleatorioCalidad = Utils.random.nextInt(VALOR_GENERACION);
            Impresora3D impr = new Impresora3D(i, CalidadImpresion.getCalidad(aleatorioCalidad));

            Ingeniero ing = new Ingeniero(i, impr, bloqueoIngenieros);
            service.submit(ing);
        }

        System.out.println("HILO-Principal Generando máquinas");

        for (int i = 0; i < MAQUINAS_A_GENERAR; i++) {

            // Inicializamos los ingenieros
            MaquinaPostProcesado maquina = new MaquinaPostProcesado(i, modelosFinalizados, bloqueoMaquinas, service);
            executor.execute(maquina);

        }

        System.out.println("HILO-Principal Espera para parar a los ingenieros");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion5.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            //No es necesario tratar la excepción puesto que el hilo principal no se va a interrumpir.
            Logger.getLogger(Sesion5.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("HILO-Principal Procesados " + modelosFinalizados.size() + " modelos.");

    }

}
