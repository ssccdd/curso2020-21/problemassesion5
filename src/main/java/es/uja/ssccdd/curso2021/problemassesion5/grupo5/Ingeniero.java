/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo5;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.random;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.TIEMPO_ESPERA_INGENIERO;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.MODELOS_A_GENERAR_MAX;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo5.Utils.MODELOS_A_GENERAR_MIN;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Ingeniero implements Callable<List<Modelo>> {

    private final int iD;
    private final Impresora3D impresora;
    private final ReentrantLock bloqueoModelosImpresos;
    private static int siguienteID = 0;

    public Ingeniero(int iD, Impresora3D impresora, ReentrantLock bloqueoModelosImpresos) {
        this.iD = iD;
        this.impresora = impresora;
        this.bloqueoModelosImpresos = bloqueoModelosImpresos;
    }

    private static int getSiguienteID() {
        return siguienteID++;
    }

    @Override
    public List<Modelo> call(){

        int numeroModelos = random.nextInt(MODELOS_A_GENERAR_MAX - MODELOS_A_GENERAR_MIN) + MODELOS_A_GENERAR_MIN;
        ArrayList<Modelo> modelosImpresos = new ArrayList<>();

        System.out.println("El ingeniero " + iD + " ha comenzado el trabajo.");

        for (int i = 0; i < numeroModelos; i++) {

            bloqueoModelosImpresos.lock();
            int idModelo = getSiguienteID();
            bloqueoModelosImpresos.unlock();

            Modelo m = new Modelo(iD, impresora.getCalidadImpresion());
            modelosImpresos.add(m);

            try {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_INGENIERO);
            } catch (InterruptedException ex) {
                //No hacemos nada porque el hilo no se interrumpirá.
                Logger.getLogger(Ingeniero.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        System.out.println("El ingeniero " + iD + " ha terminado el trabajo.\tTotal modelos impresos: " + modelosImpresos.size());

        return modelosImpresos;
    }

}
