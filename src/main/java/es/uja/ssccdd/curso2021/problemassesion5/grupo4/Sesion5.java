/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo4;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.REPARTIDORES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.RESTAURANTES_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo4.Utils.TIEMPO_ESPERA_HILO_PRINCIPAL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class Sesion5 {

    public static void main(String[] args) {
        // Variables aplicación
        int idMenus = 0;
        int idRepartidores = 0;
        int idRestaurante = 0;

        ExecutorService executor = (ExecutorService) Executors.newCachedThreadPool();
        CompletionService<List<Plato>> service = new ExecutorCompletionService<>(executor);

        ReentrantLock cerrojoRestaurantes = new ReentrantLock();
        ReentrantLock cerrojoRepartidores = new ReentrantLock();
        ArrayList<MenuReparto> menus = new ArrayList<>();

        // Ejecución del hilo principal
        System.out.println("HILO-Principal Ha iniciado la ejecución");

        System.out.println("HILO-Principal Generando restaurantes");

        for (int j = 0; j < RESTAURANTES_A_GENERAR; j++) {

            service.submit(new Restaurante(idRestaurante++, cerrojoRestaurantes));

        }

        System.out.println("HILO-Principal Generando restaurantes");
        for (int i = 0; i < REPARTIDORES_A_GENERAR; i++) {

            // Inicializamos los repartidores
            executor.execute(new Repartidor(idRepartidores++, menus, cerrojoRepartidores, service));

        }

        System.out.println("HILO-Principal Espera para parar a los repartidores");

        try {
            TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_HILO_PRINCIPAL);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion5.class.getName()).log(Level.SEVERE, null, ex);
        }

        executor.shutdownNow();

        try {
            executor.awaitTermination(TIEMPO_ESPERA_HILO_PRINCIPAL, TimeUnit.DAYS);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion5.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println("HILO-Principal Ha finalizado la ejecución");

        System.out.println("HILO-Principal Se han procesado " + menus.size() + " menús.");
    }

}
