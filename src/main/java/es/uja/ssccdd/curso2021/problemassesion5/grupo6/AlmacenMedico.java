/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2021.problemassesion5.grupo6;

import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.DOSIS_A_GENERAR;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.FabricanteVacuna;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import static es.uja.ssccdd.curso2021.problemassesion5.grupo6.Utils.TIEMPO_ESPERA_ALMACEN;
import java.util.List;
import java.util.concurrent.Callable;

/**
 *
 * @author Adrian Luque Luque (alluque)
 */
public class AlmacenMedico implements Callable<List<DosisVacuna>> {

    private final int iD;
    private final FabricanteVacuna fabricante;
    private final ReentrantLock cerrojo;
    private static int siguienteID = 0;

    public AlmacenMedico(int iD, FabricanteVacuna fabricante, ReentrantLock cerrojo) {
        this.iD = iD;
        this.fabricante = fabricante;
        this.cerrojo = cerrojo;
    }

    private static int getSiguienteID() {
        return siguienteID++;
    }

    @Override
    public List<DosisVacuna> call() {

        ArrayList<DosisVacuna> dosis = new ArrayList<>();
        System.out.println("Almacén " + iD + " ha empezado.");

        for (int i = 0; i < DOSIS_A_GENERAR; i++) {

            cerrojo.lock();
            int idDosis = getSiguienteID();
            cerrojo.unlock();
            
            dosis.add(new DosisVacuna(iD, fabricante));
            
            try {
                TimeUnit.MILLISECONDS.sleep(TIEMPO_ESPERA_ALMACEN);
            } catch (InterruptedException ex) {
                //Los hilos no van a ser interrumpidos
                Logger.getLogger(AlmacenMedico.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        System.out.println("Almacén " + iD + " ha terminado.");
        
        return dosis;
        
    }

}
