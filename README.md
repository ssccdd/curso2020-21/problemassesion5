[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 5

Problemas propuestos para la Sesión 5 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2020-21.

Esta es la segunda sesión en el que se exploran las capacidades de las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas. Las tareas definirán clases que implementen la interface [`Callable<V>`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Callable.html) y la interface `Runnable` para añadirlas al **marco de ejecución**. 

Los **objetivos** de la práctica son:

-   Definir adecuadamente las clases que implementan lo que se ejecutarán en los marcos de ejecución. Programando adecuadamente la excepción de interrupción de su ejecución.
- Crear los marcos de ejecución que se necesiten y añadir las tareas para su ejecución así como la sincronización que se pida utilizando las capacidades del marco de ejecución para ello.
- Utilizar las capacidades del marco de ejecución para la planificación temporal de tareas, recogida de resultados y cancelación de tareas.
-   Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.
    -   Realizará la prueba de las tareas definidas en el ejercicio y la finalización correcta de los diferentes marcos de ejecución que sean necesarios.
    - Debe garantizar que todas las tareas están finalizadas antes de dar por terminada su ejecución.

Los ejercicios son diferentes para cada grupo:

-   [Grupo 1](https://gitlab.com/ssccdd/curso2020-21/problemassesion5/-/blob/master/README.md#grupo-1)
-   [Grupo 2](https://gitlab.com/ssccdd/curso2020-21/problemassesion5/-/blob/master/README.md#grupo-2)
-   [Grupo 3](https://gitlab.com/ssccdd/curso2020-21/problemassesion5/-/blob/master/README.md#grupo-3)
-   [Grupo 4](https://gitlab.com/ssccdd/curso2020-21/problemassesion5/-/blob/master/README.md#grupo-4)
-   [Grupo 5](https://gitlab.com/ssccdd/curso2020-21/problemassesion5/-/blob/master/README.md#grupo-5)
-   [Grupo 6](https://gitlab.com/ssccdd/curso2020-21/problemassesion5/-/blob/master/README.md#grupo-6)

### Grupo 1
En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la creación de los diferentes procesos del sistema que se repetirá cada cierto tiempo, una vez que estén creados los procesos serán asignados por los gestores, según la disponibilidad, y una última tarea que permitirá la finalización de todas las tareas activas pasado un tiempo establecido. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ScheduledExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html) y [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html) para la ejecución de las diferentes tareas.

Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `Ordenador`, `CrearProceso`, `Informe` y `TareaFinalizacion`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `NuevoProceso` : En el constructor será necesario un `CompletionService` para la ejecución de las tareas de creación de procesos y una lista de `Future` para almacenar esas tareas que posteriormente pueden ser canceladas:
	-   Crea una tarea `CrearProceso` con un tipo aleatorio de `TipoProceso` a crear.
	-   Añade la referencia `Future` a la lista de tareas para su posterior cancelación.
	-   Hay que programar un procedimiento de interrupción cuando se solicite desde `TareaFinalizacion`.

- `GestorMemoria` : Es el encargado de asignar un `Proceso` a un ordenador de los que tiene asignados. La clase tiene las siguientes variables de instancia:
	-   Un identificador
	- Un `CompletionService` para obtener los procesos creados.

	La tarea que debe realizar es la siguiente:
	- Crea una lista de ordenadores a la que tendrá que asignar los procesos.
	- Crea una lista para añadir los procesos no asignados.
	- Mientras no se alcance el número de fallos establecido:
		- Espera a tener  un `Proceso` del `CompletionService`.
		- Para cada `Proceso` se buscará un ordenador donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre los diferentes ordenadores. Se simulará un tiempo aleatorio, atendiendo al tipo de proceso, para la realización de esta operación.
			- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de proceso.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de procesos a los ordenadores y los procesos no asignados. Si se ha interrumpido la ejecución de la tarea debe quedar reflejado en el informe.

- `Hilo princial`: Realizará los siguientes pasos:
	-   Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	-   Se crea un `CompletionService` donde se ejecutarán las tareas de `CrearProceso` y se obtienen los `Proceso` creados. La capacidad que tendrá viene determinada por la constante `PROCESOS`.
	-   Se crea un número aleatorio `NuevoProceso`, que como máximo coincide con el número de gestores, y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `CREAR_PROCESO`. La unidad de tiempo es en segundos.
	-   Se crean los `GestorMemoria` que se indican por la constante `NUM_GESTORES` y se añade para su ejecución desde el comienzo.
	-   Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	-   El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	-   Cierra los marcos de ejecución.
	-   Presenta los informes de los gestores.

### Grupo 2
En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la creación de los diferentes procesos del sistema que se repetirá cada cierto tiempo, una vez que estén creados los procesos serán asignados por los gestores, según la disponibilidad, y una última tarea que permitirá la finalización de todas las tareas activas pasado un tiempo establecido. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ScheduledExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html) y [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html) para la ejecución de las diferentes tareas.

Para la realización del ejercicio se hará uso de las clases ya definidas: `Proceso`, `ColaPrioridad`, `CrearProceso`, `Informe` y `TareaFinalizacion`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `NuevoProceso` : En el constructor será necesario un `CompletionService` para la ejecución de las tareas de creación de procesos y una lista de `Future` para almacenar esas tareas que posteriormente pueden ser canceladas:
	-   Crea una tarea `CrearProceso` y se añade para su ejecución.
	-   Añade la referencia `Future` a la lista de tareas para su posterior cancelación.
	-   Hay que programar un procedimiento de interrupción cuando se solicite desde `TareaFinalizacion`.

- `GestorProcesos` : Es el encargado de asignar un `Proceso` a una de las colas que tiene asignadas. La clase tiene las siguientes variables de instancia:
	-   Un identificador
	- Un `CompletionService` para obtener los procesos creados.

	La tarea que debe realizar es la siguiente:
	- Crea una lista de colas de prioridad a la que tendrá que asignar los procesos.
	- Crea una lista para añadir los procesos no asignados.
	- Mientras no se alcance el número de fallos establecido:
		- Espera a tener un `Proceso` del `CompletionService`.
		- Para cada `Proceso` se buscará una cola de prioridad donde asignarlo, hay que procurar que la carga de procesos esté balanceada entre las diferentes colas. Se simulará un tiempo aleatorio, atendiendo al tipo de prioridad, para la realización de esta operación.
			- Si no ha podido ser asignado el proceso se añadirá a la lista de procesos no asignados. Esto cuenta como un fallo.
	- Programar la interrupción completando la última asignación de proceso.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de procesos a las colas con prioridad y los procesos no asignados. Si se ha interrumpido la ejecución de la tarea debe quedar reflejado en el informe.

- `Hilo princial`: Realizará los siguientes pasos:
	-   Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	-   Se crea un `CompletionService` donde se ejecutarán las tareas de `CrearProceso` y se obtienen los `Proceso` creados. La capacidad que tendrá viene determinada por la constante `PROCESOS`.
	-   Se crea un número aleatorio `NuevoProceso`, que como máximo coincide con el número de gestores, y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `CREAR_PROCESO`. La unidad de tiempo es en segundos.
	-   Se crean los `GestorProcesos` que se indican por la constante `NUM_GESTORES` y se añade para su ejecución desde el comienzo.
	-   Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	-   El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	-   Cierra los marcos de ejecución.
	-   Presenta los informes de los gestores.

### Grupo 3
En el ejercicio utilizaremos diferentes tareas y las posibilidades de la *ejecución planificada*. En el sistema dispondremos de tareas especializadas para: la creación de los diferentes procesos del sistema que se repetirá cada cierto tiempo, una vez que estén creados los procesos serán asignados por los gestores, según la disponibilidad, y una última tarea que permitirá la finalización de todas las tareas activas pasado un tiempo establecido. Las herramientas que utilizaremos para la solución del ejercicio serán: [`ScheduledExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ScheduledExecutorService.html) y [`CompletionService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/CompletionService.html) para la ejecución de las diferentes tareas.

Para la realización del ejercicio se hará uso de las clases ya definidas: `Archivo`, `UnidadAlmacenamiento`, `CrearArchivo`, `Informe` y `TareaFinalizacion`. Además en la interface `Constantes` también deberá utilizarse en el desarrollo del ejercicio. 

Hay que completar la implementación de las siguientes clases:

 - `NuevoArchivo` : En el constructor será necesario un `CompletionService` para la ejecución de las tareas de creación de archivos y una lista de `Future` para almacenar esas tareas que posteriormente pueden ser canceladas:
	-   Crea una tarea `CrearArchivo` y la añade para su ejecución.
	-   Añade la referencia `Future` a la lista de tareas para su posterior cancelación.
	-   Hay que programar un procedimiento de interrupción cuando se solicite desde `TareaFinalizacion`.

- `GestorAlmacenamiento` : Es el encargado de asignar un `Archivo` a una unidad de almacenamiento que tiene asignadas. La clase tiene las siguientes variables de instancia:
	-   Un identificador
	- Un `CompletionService` para obtener los archivos creados.

	La tarea que debe realizar es la siguiente:
	- Crea una lista de unidades de almacenamiento a la que tendrá que asignar los archivos.
	- Crea una lista para añadir los archivos no asignados.
	- Mientras no se alcance un número de archivos almacenados establecido:
		- Espera a tener un  `Archivo`  del  `CompletionService`.
		- Para cada `Archivo` se buscará la primera unidad de almacenamiento donde asignarlo. Se simulará un tiempo aleatorio, atendiendo al tipo de archivo, para la realización de esta operación.
			- Si no ha podido ser asignado el archivo se añadirá a la lista de archivos no asignados.
	- Programar la interrupción completando la última asignación de archivo.
	- El resultado de la ejecución de esta tarea es crear un `Informe` donde se debe mostrar el estado de asignación de los archivos y los archivos no asignados. Si se ha interrumpido la ejecución de la tarea debe quedar reflejado en el informe.

- `Hilo princial`: Realizará los siguientes pasos:
	-   Se crea un `ScheduledExecutorService` para las tres tareas especializadas del sistema.
	-   Se crea un `CompletionService` donde se ejecutarán las tareas de `CrearArchivo` y se obtienen los `Archivo` creados. La capacidad que tendrá viene determinada por la constante `ARCHIVOS`.
	-   Se crea un número aleatorio `NuevoArchivo`, que como máximo coincide con el número de gestores, y se añade para que se ejecute desde el comienzo y se repita cada periodo definido en `CREAR_ARCHIVO`. La unidad de tiempo es en segundos.
	-   Se crean los `GestorAlmacenamiento` que se indican por la constante `NUM_GESTORES` y se añade para su ejecución desde el comienzo.
	-   Se crea una tarea `TareaFinalizacion` y se añade para su ejecución pasado un tiempo definido en `TIEMPO_ESPERA`. La unidad de tiempo es en minutos.
	-   El hilo principal espera a la finalización de `TareaFinalización` mediante el elemento de sincronización compartido.
	-   Cierra los marcos de ejecución.
	-   Presenta los informes de los gestores.


## Grupo 4

Se ha decidido que la forma anterior de trabajar con la gestión de los menús no era eficiente. Para optimizar el tiempo, primero los `restaurantes` generarán todos los `platos` necesarios, cada restaurante generará todos los platos de un menú. Para que posteriormente los `repartidores` puedan recoger esos platos y rellenar `MenuReparto`. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor` que permita generar tantos hilos como sea necesario y un `CompletionService` que gestione los conjuntos de platos que genera cada restaurante y los sirva a los repartidores que esperan esos conjuntos. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `Plato`: Clase que representa a un plato de tipo `TipoPlato`.
- `MenuReparto`: Representa a un pedido, puede incluir un plato de cada uno de los presentes en `TipoPlato`, pero no más de uno de cada tipo.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `Restaurante`:
	- Los restaurantes guardarán en variables de instancia, su identificador, una variable compartida para asignar IDs diferentes y un elemento de sincronización acceder a esta variable. En la clase Restaurante hay implementado un método que permite obtener IDs únicos, pero hace falta acceder en exclusión mutua. 
	- Cada restaurante fabricará tanto platos como sean necesarios para rellenar un menú, cada uno necesita un ID único.
	- Entre la generación de cada plato, el restaurante esperará `TIEMPO_ESPERA_RESTAURANTE` milisegundos.
	- Al finalizar el restaurante devolverá un vector de platos.
- `Repartidor`:
	- El repartidor guardará en variables de instancia, su identificador, un `CompletionService` al que solicitar los platos que generan los restaurantes, una variable compartida para asignar IDs diferentes, un array compartido para los `MenuReparto` que se vayan produciendo y un elemento de sincronización para el array y el acceso al generador de IDs. En la clase Repartidor hay implementado un método que permite obtener IDs únicos, pero hace falta acceder en exclusión mutua.
	- El repartidor estará esperando vectores de platos de forma indefinida, hasta que sea interrumpido. Para obtener los platos usará el método .take() del `CompletionService`, este método espera a que haya `Future` disponibles y se puede interrumpir.
	- Por cada paquete de trabajo recibido (platos para rellenar un pedido):
		- Generar un menú con un ID único.
		- Insertar los platos en el menú.
		- Guardarlo en el array común de pedidos.
		- Esperar entre `TIEMPO_ESPERA_REPARTIDOR_MIN` y `TIEMPO_ESPERA_REPARTIDOR_MAX` milisegundos.
	- Al finalizar, imprimirá los datos de los pedidos que ha generado, **solo los generados por cada repartidor**.
- `Hilo Principal`:
	- Genera un array de pedidos común y los elementos de sincronización.
	- Genera el gestor/ejecutor de tareas y el `CompletionService`.
	- Crea e inicia la ejecución de `RESTAURANTES_A_GENERAR` restaurantes, asociándolos al `CompletionService`.
	- Crea e inicia la ejecución de `REPARTIDORES_A_GENERAR` repartidores usando un gestor de hilos.
	- Espera un tiempo, definido por TIEMPO_ESPERA_HILO_PRINCIPAL en milisegundos.
	- Hace un `shutdownNow()` del gestor de hilos para interrumpir a los repartidores y espera a que acaben con `awaitTermination()`. El shutdownNow() es equivalente al .interrupt() usado anteriormente, y se aplica a todos los hilos "vivos" gestionados por el Executor.
	- Al finalizar, imprime el número de menús generados en total.


## Grupo 5

Se ha decidido que la forma anterior de trabajar con la gestión de la impresión y postprocesado de modelos no era eficiente. Para optimizar el tiempo, primero los `ingenieros` generan modelos impresos, para que posteriormente `MáquinaPostprocesado` reciban y procesen los modelos. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de `Executor` que permita generar tantos hilos como sea necesario y un `CompletionService` que gestione los conjuntos de modelos impresos para pasarlos a las máquinas. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `Modelo`: Clase que representa a un modelo 3D con unos requisitos de tipo `CalidadImpresion`.
- `Impresora3D`: Representa a una impresora 3D y una `CalidadImpresion` que puede generar la impresora. 

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Ingeniero`:
	- Los ingenieros guardarán en variables de instancia, su identificador, una variable compartida para asignar IDs diferentes y un elemento de sincronización acceder a esta variable. En la clase Ingeniero hay implementado un método que permite obtener IDs únicos, pero hace falta acceder en exclusión mutua.
	- Imprimirá entre `MODELOS_A_GENERAR_MIN` y `MODELOS_A_GENERAR_MAX` modelos:
		- La calidad de los modelos vendrá determinada por la calidad de la impresora asignada.
		- Después de crear el modelo el ingeniero esperará `TIEMPO_ESPERA_INGENIERO` milisegundos.	
	- Al terminar se mostrarán cuantos modelos se han impreso y se hará un return de los modelos impresos.
 - `MaquinaPostprocesado`:
   	-  Las máquinas guardarán en variables de instancia, su identificador, un `CompletionService` al que solicitar los modelos que imprimirán los ingenieros, un array compartido para los `Modelos` que se vayan procesando y un elemento de sincronización para el acceso al array. 
   	- La máquina estará esperando vectores de modelos de forma indefinida, hasta que sea interrumpida. Para obtener los modelos usará el método .take() del `CompletionService`, este método espera a que haya `Future` disponibles y se puede interrumpir.
   	- Para cada paquete de trabajo de modelos impresos:
   		- Esperar entre cada modelo, entre `TIEMPO_ESPERA_MAQUINA_MIN` y `TIEMPO_ESPERA_MAQUINA_MAX` milisegundos.
   		- Introducir el modelo en el array de modelos procesados.
   		- Si la máquina es interrumpida durante el procesamiento de un paquete de trabajo, se debe de terminar de procesar los modelos y luego interrumpirse.
	- Al finalizar, imprimirá los datos de los modelos que ha procesado, **solo los procesados por cada máquina**.
- `Hilo Principal`:
	-   Genera un array de modelos procesados común y los elementos de sincronización.
	-   Genera el gestor/ejecutor de tareas y el `CompletionService`.
	-   Crea e inicia la ejecución de `INGENIEROS_A_GENERAR` ingenieros, asociándolos al `CompletionService`.
	-   Crea e inicia la ejecución de `MAQUINAS_A_GENERAR` máquinas usando un gestor de hilos.
	- 	Espera un tiempo, definido por TIEMPO_ESPERA_HILO_PRINCIPAL en milisegundos.
	-   Hace un `shutdownNow()` del gestor de hilos para interrumpir a las máquinas y espera a que acaben con `awaitTermination()`. El shutdownNow() es equivalente al .interrupt() usado anteriormente, y se aplica a todos los hilos “vivos” gestionados por el Executor.
	-   Al finalizar, imprime el número de modelos procesados en total.


## Grupo 6

Se ha decidido que la forma anterior de trabajar con la gestión de la vacunación no era eficiente. Para optimizar el tiempo, primero los `almacenes médicos` generarán todas las `dosisVacuna` necesarias en lotes, para que posteriormente los `enfermeros` puedan vacunar a los pacientes disponibles usando esos lotes. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de `Executor` que permita generar tantos hilos como sea necesario y un `CompletionService` que gestione los paquetes de dosis que genera cada almacén y los sirva a los enfermeros que esperan esas dosis. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten.

- `Utils`: Clase con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta clase de forma obligatoria para la resolución.
- `DosisVacuna`: Clase que representa a cada dosis de la vacuna de un `FabricanteVacuna` determinado.
- `Paciente`: Representa a un paciente al que se le pueden administrar hasta dos dosis siempre que las dos sean del mismo fabricante.

El ejercicio consiste en completar la implementación de las siguientes clases:
- `AlmacénMédico`:
	- Los almacenes guardarán en variables de instancia, su identificador, el `FabricanteVacuna` que puede producir, una variable compartida para asignar IDs diferentes y un elemento de sincronización acceder a esta variable. En la clase AlmacénMédico hay implementado un método que permite obtener IDs únicos, pero hace falta acceder en exclusión mutua.
	- Cada tipo de almacén fabricará `DOSIS_A_GENERAR` dosis de su tipo.
	- Entre la generación de cada dosis, el almacén esperará `TIEMPO_ESPERA_ALMACEN` milisegundos.
	- Para simplificar el problema, el número de dosis será múltiplo del número de dosis necesarias para cada paciente, de tal forma que un paquete de trabajo permite inmunizar a una serie de pacientes sin dejar a ninguno con falta de dosis.
- `Enfermero`:
	- El enfermero guardará en variables de instancia, su identificador, un array de pacientes sin inmunizar, un array de pacientes inmunizados, y elementos de sincronización para los array. Los arrays anteriores serán compartidos con todos los enfermeros.
	- El enfermero estará esperando vectores de dosis de forma indefinida, hasta que sea interrumpido. Para obtener las dosis usará el método .take() del `CompletionService`, este método espera a que haya `Future` disponibles y se puede interrumpir.
	- Para paquete de trabajo de dosis generadas:
		- Se iterará sobre los pacientes disponibles en el array de pacientes, sacando el paciente del array, inyectando las dosis necesarias y guardando al paciente en el array de pacientes inmunizados.
		- Entre cada dosis administrada se esperará entre `TIEMPO_ESPERA_ENFERMERO_MIN` y `TIEMPO_ESPERA_ENFERMERO_MAX` milisegundos.
		- Si el enfermero es interrumpido durante el procesamiento de un paquete de dosis debe de asegurarse que el paciente actual recibe todas las dosis, pero interrumpirá el trabajo dejando las dosis restantes sin administrar.
		- Si no quedan pacientes en la lista, se debe de interrumpir el hilo.
	- Al finalizar, imprimirá los datos de los pacientes inmunizados, **solo los inmunizados por el enfermero**.
- `Hilo Principal`:
	- Genera un array común de pacientes rellenado con `PACIENTES_A_GENERAR` y un array común de pacientes inmunizados vacíos y los elementos de sincronización.
	- Genera el gestor/ejecutor de tareas y el `CompletionService`.
	- Crea e inicia la ejecución de `ALMACENES_A_GENERAR`, asociándolos al `CompletionService`. Se tiene que crear un tercio de almacenes de cada tipo de fabricante.
	- Crea e inicia la ejecución de `ENFERMEROS_A_GENERAR` enfermeros usando un gestor de hilos.
	- Espera un tiempo, definido por TIEMPO_ESPERA_HILO_PRINCIPAL en milisegundos.
	- Hace un `shutdownNow()` del gestor de hilos para interrumpir a los enfermeros y espera a que acaben con `awaitTermination()`. El shutdownNow() es equivalente al .interrupt() usado anteriormente, y se aplica a todos los hilos “vivos” gestionados por el Executor.
	- Al finalizar, imprime el número de pacientes inmunizados en total.
